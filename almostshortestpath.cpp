#include <iostream>
#include <list>
#include <queue>
#include <set>
#include <stack>

#define MAX_WEIGHT 1000001

using namespace std;

struct node{
    unsigned int nodeNumber;
    list<unsigned int> previousDijkstraNodes;
    unsigned int minCost;
    bool seen;

    bool operator <(const node & other) const{
        return minCost < other.minCost;
    }

    bool operator >(const node &other) const{
        return minCost > other.minCost;
    }
    node():nodeNumber(0),previousDijkstraNodes({}),minCost(0),seen(false){};

    explicit node(unsigned int _nodeNumber, unsigned int _minCost = MAX_WEIGHT):nodeNumber(_nodeNumber),previousDijkstraNodes({}),minCost(_minCost),seen(false){};
};

int main(){
    unsigned int nodesNumber, directRoutesNumber;
    while(scanf("%u %u",&nodesNumber,&directRoutesNumber) != EOF){
        if(nodesNumber == 0 && directRoutesNumber == 0)
            break;
        unsigned int startingNode, endNode;
        deque<node> nodes;
        priority_queue<node,vector<node>,greater<node>> nodeCostQueue;
        int * pathWeights = new int[nodesNumber*nodesNumber];
        for(unsigned int i = 0; i < nodesNumber ; i++){
            for(unsigned int j = 0 ; j < nodesNumber ; j++){
                pathWeights[i*nodesNumber+j] = -1;
            }
        }
        scanf("%u %u",&startingNode, &endNode);
        for(unsigned int i = 0 ; i < nodesNumber ; i++){
            if(i == startingNode) {
                node newNode(i,0);
                nodeCostQueue.push(newNode);
                nodes.push_back(newNode);
            }else {
                node newNode(i);
                nodeCostQueue.push(newNode);
                nodes.push_back(newNode);
            }
        }
        for(unsigned int i = 0 ; i < directRoutesNumber ; i++){
            unsigned int beginPathNode, endPathNode, weight;
            scanf("%u %u %u",&beginPathNode, &endPathNode, &weight);
            pathWeights[beginPathNode*nodesNumber+endPathNode] = weight;
        }
        //Dijkstra
        while(!nodeCostQueue.empty()){
            node currentNode = nodeCostQueue.top();
            nodeCostQueue.pop();
            unsigned int nodeNumber = currentNode.nodeNumber;
            if(!nodes[nodeNumber].seen){
                for(unsigned int j = 0 ; j < nodesNumber; j++){
                    int distanceFromNode = pathWeights[nodeNumber*nodesNumber+j];
                    if(distanceFromNode != -1){
                        if(nodes[nodeNumber].minCost + distanceFromNode < nodes[j].minCost){
                            nodes[j].minCost = currentNode.minCost + distanceFromNode;
                            nodes[j].previousDijkstraNodes = {currentNode.nodeNumber};
                            nodeCostQueue.push(nodes[j]);
                        }else if(currentNode.minCost + distanceFromNode == nodes[j].minCost){
                            nodes[j].previousDijkstraNodes.push_back(currentNode.nodeNumber);
                        }
                        nodes[nodeNumber].seen = true;
                    }
                }
            }
        }
        //Remove all the involved networks
        stack<node> nodesToRemove;
        nodesToRemove.push(nodes[endNode]);
        while(!nodesToRemove.empty()){
            node endLinkNode = nodesToRemove.top();
            nodesToRemove.pop();
            for(list<unsigned int>::const_iterator ite = endLinkNode.previousDijkstraNodes.cbegin(); ite != endLinkNode.previousDijkstraNodes.cend();ite++){
                pathWeights[(*ite)*nodesNumber+endLinkNode.nodeNumber] = -1;
                nodesToRemove.push(nodes[*ite]);
            }
        }
        //Dijkstra
        nodes.clear();
        for(unsigned int i = 0; i < nodesNumber; i++){
            for(unsigned int i = 0 ; i < nodesNumber ; i++){
                if(i == startingNode) {
                    node newNode(i,0);
                    nodeCostQueue.push(newNode);
                    nodes.push_back(newNode);
                }else {
                    node newNode(i);
                    nodeCostQueue.push(newNode);
                    nodes.push_back(newNode);
                }
            }
        }
        while(!nodeCostQueue.empty()) {
            node currentNode = nodeCostQueue.top();
            nodeCostQueue.pop();
            unsigned int nodeNumber = currentNode.nodeNumber;
            if (!nodes[nodeNumber].seen) {
                for (unsigned int j = 0; j < nodesNumber; j++) {
                    int distanceFromNode = pathWeights[nodeNumber * nodesNumber + j];
                    if (distanceFromNode != -1) {
                        if (nodes[nodeNumber].minCost + distanceFromNode < nodes[j].minCost) {
                            nodes[j].minCost = currentNode.minCost + distanceFromNode;
                            nodeCostQueue.push(nodes[j]);
                            nodes[nodeNumber].seen = true;
                        }
                    }
                }
            }
        }
        if(nodes[endNode].minCost == MAX_WEIGHT)
            printf("%d\n",-1);
        else
            printf("%u\n",nodes[endNode].minCost);
    }
    return 0;
}
