#include <iostream>
#include <cmath>
#include <algorithm>
#include <cfloat>

using namespace std;

struct vector3D{
    int x,y,z;
    vector3D():x(0),y(0),z(0){};
    vector3D(int _x, int _y, int _z):x(_x),y(_y),z(_z){};
    inline double distance(const vector3D other) const{
        return sqrt((other.x-x)*(other.x-x) + (other.y - y)*(other.y - y)+(other.z - z)*(other.z-z));
    }
    inline double minRadius(const vector3D other) const{
        return min(min(abs(other.x-x),abs(other.y-y)),abs(other.z-z));
    }
    inline int volume(const vector3D other) const{
        return abs(other.x - x)*abs(other.y - y) * abs(other.z-z);
    }
};

inline double volume(double radius){
    return 4.0/3.0*M_PI*radius*radius*radius;
}

int main(){
    int pinsNumber;
    int caseNumber = 0;
    while(scanf("%d", &pinsNumber) != EOF){
        caseNumber += 1;
        if(pinsNumber == 0)
            break;
        double* radiusToWall = new double[pinsNumber];
        vector3D* pins = new vector3D[pinsNumber];
        double* distanceBetweenPin = new double[pinsNumber*pinsNumber];
        int* permutationArray = new int[pinsNumber];
        int x,y,z;
        scanf("%d %d %d",&x, &y,&z);
        vector3D firstCorner(x,y,z);
        scanf("%d %d %d",&x, &y, &z);
        vector3D secondCorner(x,y,z);
        int boxVolume = firstCorner.volume(secondCorner);
        //static distance calculations between pins and wall and with other pins
        for(int i = 0 ; i < pinsNumber; i++){
            permutationArray[i]=i;
            scanf("%d %d %d",&pins[i].x,&pins[i].y, &pins[i].z);
            radiusToWall[i] = min(pins[i].minRadius(firstCorner),pins[i].minRadius(secondCorner));
            for(int j = 0; j < i ; j++){
                distanceBetweenPin[i*pinsNumber+j] = pins[i].distance(pins[j]);
            }
        }
        //enumerating possibilities
        double maxVolume = 0;
        do{
            double currentVolume = 0;
            double* currentRadius = new double[pinsNumber];
            for(int i = 0; i < pinsNumber; i++){
                double minBalloonsRadius = DBL_MAX;
                for(int j = 0 ; j < i ; j++){
                    int maxI = max(permutationArray[i], permutationArray[j]);
                    int minJ = min(permutationArray[i], permutationArray[j]);
                    minBalloonsRadius = min(minBalloonsRadius,distanceBetweenPin[maxI*pinsNumber+minJ]-currentRadius[j]);
                    if(minBalloonsRadius < 0)
                        break;
                }
                if(minBalloonsRadius < 0)
                    currentRadius[i] = 0;
                else if(i != 0) {
                    currentRadius[i] = min(minBalloonsRadius, radiusToWall[permutationArray[i]]);
                    currentVolume += volume(currentRadius[i]);
                }
                else {
                    currentRadius[i] = radiusToWall[permutationArray[i]];
                    currentVolume += volume(currentRadius[i]);
                }
            }
            maxVolume = max(maxVolume,currentVolume);
        }while(next_permutation(permutationArray,permutationArray+pinsNumber));
    printf("Box %d: %d\n\n",caseNumber, int(round(boxVolume-maxVolume)));
    }
    return 0;
}