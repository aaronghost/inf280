//We pre-compute the values of 1 under each power of 2
//Then we look up in this table and add the result based on the decomposition in base two of one number
#include <iostream>

using namespace std;
unsigned long long onesBeforeThis[55];
unsigned long long currentTwoPower[55];

unsigned long long numberOfOneIn(unsigned long long value){
    unsigned long long ans = 0;
    bool valueInBaseTwo[55] ={false};
    //Decompose the number on base two
    for(unsigned int i = 0; i < 54; i++){
        valueInBaseTwo[i] = value%2;
        value = value/2;
    }
    //This number represents the number of ones that come from the first ignored digit in the pre-computation
    //The digits there are the one that come from the ones in the numeral representation that we have seen before
    unsigned int formerNumberOfOne = 0;
    for(int i = 54; i >= 0; i--){
        if(valueInBaseTwo[i]){
            ans += onesBeforeThis[i] + currentTwoPower[i]*formerNumberOfOne;
            formerNumberOfOne+=1;
        }
    }
    ans += formerNumberOfOne;
    return ans;
}

int main(){

    //Pre-computation
    onesBeforeThis[0] = 0;
    currentTwoPower[0] = 1;
    for(unsigned int i = 1; i < 55; i++){
        //The numbers before 2**n=(1n 0n-1 ... 00) can be divided in two categories
        //The numbers which are bigger or equal to 2**(n-1) (0n 1n-1 ... 00)
        //The numbers which are smaller to 2**(n-1)
        //In the two groups there are exactly 2**(n-1) numbers
        //So that the numbers of ones is the same between the two groups except the ones which are on the (n-1)th position
        //From that we get the following recurring formula : |x(n)| = |x(n-1)|*2+2**(n-1)
        onesBeforeThis[i] = 2 * onesBeforeThis[i - 1] + currentTwoPower[i - 1];
        currentTwoPower[i] = currentTwoPower[i-1]*2;
    }
    //using the formula
    unsigned long long A,B;
    while(scanf("%llu %llu",&A,&B) != EOF){
        printf("%llu\n",numberOfOneIn(B)-numberOfOneIn(A-1));
    }


    return 0;
}
