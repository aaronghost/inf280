#include <iostream>

using namespace std;

int main(){
    string line;
    unsigned long threeModulo[3];
    while(getline(cin,line)){
        unsigned long long result = 0;
        threeModulo[0] = 1;
        threeModulo[1] = 0;
        threeModulo[2] = 0;
        char currentState = 0;
        for(int i = 0; i < line.size(); i++){
            //no-digits strip
            char newChar = line[i];
            if(newChar > 58) {
                threeModulo[0] = 1;
                threeModulo[1] = 0;
                threeModulo[2] = 0;
                currentState = 0;
            }else {
                currentState = char((currentState + newChar) % 3);
                result += threeModulo[currentState];
                threeModulo[currentState] += 1;
            }
        }
        printf("%llu\n",result);
    }
    return 0;
}