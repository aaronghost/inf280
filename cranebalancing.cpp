//Wikipedia centroid formula for non-self intersecting polygon
//n being the number of points (x,y the given coordinates)
//CX = (1/(6A) SUM(xi + x(i+1))(xi*y(i+1)-x(i+1)*yi)
//Here we don't care about y but for knowledge
//CY = (1/6A) SUM(yi + y(i+1))(xi*y(i+1)-x(i+1)*yi)
//Where A is the signed area
//A = 1/2 SUM(xi*y(i+1)-x(i+1)yi)

//Cendroid formula for two weighted mass nodes m1 and m2
//Cx = 1/(m1+m2)*(m1*x1 + m2*x2)
//m2 = m1(x1 - Cx)/(Cx - x2)


#include <iostream>
#include <cmath>
#include <vector>

using namespace std;

struct Point{
    int x;
    int y;

    double cross_product(const Point &other) const{
        return double(x*other.y - y*other.x);
    }

    Point()= default;

    Point(int _x, int _y):x(_x),y(_y){}};

int main(){
    unsigned int vertexNumber;
    while(scanf("%u",&vertexNumber) != EOF){
        int base_max = -2000, base_min = 2000;
        vector<Point> points;
        double area = 0;
        double centroid_x = 0;
        for(unsigned int i = 0; i < vertexNumber; i++){
            int x;
            int y;
            scanf("%d %d",&x,&y);
            points.emplace_back(x,y);
            //test if the point is in the base and updates the base extremum
            if(y == 0){
                base_max = max(x,base_max);
                base_min = min(x,base_min);
            }
        }
        points.push_back(points[0]);
        //Compute area and centroid of the structure
        for(unsigned int i =0; i < vertexNumber; i++){
            //last value is taken with the first point
            area += points[i].cross_product(points[i+1]);
            centroid_x += points[i].cross_product(points[i+1])*(points[i].x+points[i+1].x);
        }
        //We now compute the full centroid from the area value (positive)
        if(area < 0) {
            area = - area;
            centroid_x = - centroid_x;
        }
        area = area/2;
        centroid_x = centroid_x/(6*area);

        //We add weight on the right
        if(points[0].x >= base_max) {
            //The crane is already unbalanced on the right -> unstable
            if(centroid_x > base_max){
                printf("unstable\n");
            }else{
                //the crane is unbalanced at the beginning
                if(centroid_x < base_min){
                    double min_weight = area * (centroid_x-base_min)/(base_min-points[0].x);
                    printf("%d .. ",int(floor(min_weight)));
                }else{
                    printf("0 .. ");
                }
                //We continue to add weight
                //Lucky one the point is in the base we can as much weight as we want
                if(points[0].x == base_max){
                    printf("inf\n");
                }else{
                    //same calculation as balancing unbalanced empty crane
                    double max_weight = area *(centroid_x-base_max)/(base_max - points[0].x);
                    printf("%d\n",int(ceil(max_weight)));
                }
            }
        }else if(points[0].x <= base_min) {
            //Symmetrical of before
            //The crane is already unbalanced on the left -> unstable
            if(centroid_x < base_min){
                printf("unstable\n");
            }else{
                //the crane is unbalanced at the beginning
                if(centroid_x > base_max){
                    double min_weight = area * (centroid_x-base_max)/(base_max-points[0].x);
                    printf("%d .. ",int(floor(min_weight)));
                }else{
                    printf("0 .. ");
                }
                //We continue to add weight
                //Lucky one the point is in the base we can as much weight as we want
                if(points[0].x == base_min){
                    printf("inf\n");
                }else{
                    //same calculation as balancing unbalanced empty crane
                    double max_weight = area *(centroid_x-base_min)/(base_min - points[0].x);
                    printf("%d\n",int(ceil(max_weight)));
                }
            }
        }else{
            printf("0 .. inf\n");
        }
    }
    return 0;
}