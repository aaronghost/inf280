#include <iostream>
#include <cmath>

using namespace std;

//Return the partial volume of the sphere of radius R and of height (z) => total sphere with z = 2*R
double volumePartialSphere(double R, double z){
    return M_PI/3.*z*z*(3*R-z);
}

double volume(double end, int holesNumber, const double * holesZ, const double * holesR, const double * holesV, double volumeToRemove){
    double plainVolume = end*100*100;
    double holesVolume = 0;
    for(int i = 0 ; i < holesNumber; i++){
        double z = holesZ[i];
        double r = holesR[i];
        if(z-r <= end){
            if( z + r > end)
                holesVolume += volumePartialSphere(r,end - z + r);
            else
                holesVolume += holesV[i];
        }
    }
    return plainVolume - holesVolume - volumeToRemove;
}

int main(){
    int holesNumber, slicesNumber;
    while(scanf("%d %d",&holesNumber, &slicesNumber) == 2) {
        //X, y are useless
        double* holesZ = new double[holesNumber];
        double* holesR = new double[holesNumber];
        double* holesV = new double[holesNumber];
        //get all the holes parameters and compute the size of the
        double holesVolume = 0;
        for(int i = 0; i < holesNumber; i++){
            int x,y,z,r;
            scanf("%d %d %d %d",&r,&x,&y,&z);
            holesZ[i] = double(z/1000.);
            holesR[i] = double(r/1000.);
            holesV[i] = volumePartialSphere(holesR[i],2*holesR[i]);
            holesVolume += holesV[i];
        }
        //TODO sort Z to optimise reading?
        double sliceVolume = double((pow(100,3.)- holesVolume))/double(slicesNumber);
        double begin = 0;
        double fixedBegin = 0;
        double middle = 50;
        double end = 100;
        for(int i = 0; i < slicesNumber; i++){
            double volumeToRemove = sliceVolume * double(i);
            double currentVolume = volume(middle,holesNumber,holesZ,holesR, holesV, volumeToRemove);
            while((fabs(currentVolume - sliceVolume) > 1e-6)){
                if(currentVolume < sliceVolume){
                    begin = middle;
                }else{
                    end = middle;
                }
                middle = (begin + end)/2;
                currentVolume = volume(middle,holesNumber, holesZ, holesR, holesV, volumeToRemove);
            }
            printf("%f\n",middle - fixedBegin);
            fixedBegin = middle;
            end = 100;
            middle = (fixedBegin + end)/2;
        }
    }
    return 0;
}
