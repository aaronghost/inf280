#include <iostream>
#include <list>
#include <vector>
#include <map>
#include <climits>

using namespace std;

int main(){
    unsigned int nameNumber, relationshipNumber;
    unsigned int caseNumber = 1;
    while(scanf("%u %u",&nameNumber,&relationshipNumber) != EOF){
        if(nameNumber == 0 && relationshipNumber == 0)
            break;
        char name1[51];
        char name2[51];
        vector<list<unsigned int>> adjacentNames;
        vector<vector<int>> distances;
        for(unsigned int i = 0 ; i < nameNumber; i++) {
            vector<int> distanceRow;
            for (unsigned int j = 0; j < nameNumber; j++) {
                if(i == j)
                    distanceRow.push_back(0);
                else
                    distanceRow.push_back(-1);
            }
            distances.push_back(distanceRow);
            adjacentNames.push_back({});
        }

        map<string, int> nameAssociation;
        int currentNameNumber = 0;
        for(unsigned int i = 0 ; i < relationshipNumber ; i++){
            scanf("%50s %50s",&name1[0],&name2[0]);
            int firstName, secondName;
            if(nameAssociation.count(name1) == 0){
                nameAssociation.insert(make_pair(name1,currentNameNumber));
                firstName = currentNameNumber;
                currentNameNumber++;
            }else{
                firstName = nameAssociation[name1];
            }
            if(nameAssociation.count(name2) == 0){
                nameAssociation.insert(make_pair(name2,currentNameNumber));
                secondName = currentNameNumber;
                currentNameNumber++;
            }else{
                secondName = nameAssociation[name2];
            }
            adjacentNames[firstName].push_back(secondName);
            adjacentNames[secondName].push_back(firstName);
            distances[firstName][secondName] = 1;
            distances[secondName][firstName] = 1;
        }
        //floyd-Warshall
        for(unsigned int k=0; k < nameNumber; k++)      // check sub-path combinations
            for(unsigned int i=0; i < nameNumber; i++)
                for(unsigned int j=0; j < nameNumber; j++)
                    if(distances[i][k] != -1 && distances[k][j] != -1) {// concatenate paths
                        if (distances[i][j] != -1) {
                            distances[i][j] = min(distances[i][j], distances[i][k] + distances[k][j]);
                        } else {
                            distances[i][j] = distances[i][k] + distances[k][j];
                        }
                    }


        //Maximum extraction
        bool disconnected = false;
        int maximumSeparation = 0;
        for(unsigned int i = 0 ; i < nameNumber; i++) {
            for (unsigned int j = 0; j < nameNumber; j++) {
                maximumSeparation = max(maximumSeparation, distances[i][j]);
                if (i != j && distances[i][j] == -1) {
                    disconnected = true;
                    break;
                }
            }
            if(disconnected)
                break;
        }
        if(disconnected)
            printf("Network %u: DISCONNECTED\n\n",caseNumber);
        else
            printf("Network %u: %u\n\n",caseNumber,maximumSeparation);
        caseNumber++;
    }
    return 0;
}

