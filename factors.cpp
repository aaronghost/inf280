//We precompute all the values and then we just look through the associative table
//The reason we can do that is that the problem is equivalent to look to the Newton multinomial values
//The best solution is the one that minimize n in the (n; k1 ... kj) (with n = k1 + ... + kj)
//As we are doing this with primes, we will be first computing a pseudo-Pascal's simplex with prime multiplicators
//The limits to compute those values are 2**63
//There is no dynamic value to compute but only precomputed values

//the computation works on the principle that (k1+...+kj ; k1 ... kj) = (k1+...+k(j-1);k1 .. k(j-1))*(k1+...+kj; kj)

//NB optimal solution should not be the one that we are presenting (just computing a map and throwing it to the test would be better)

#include <iostream>
#include <map>

using namespace std;

//MaxValue is 2**63
#define MAXValue (uint64_t(1) << 63)
//precomputed primes we don't want to lose time with this
unsigned int const primes[] = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97};

//truncated factors to look at one step (too big values are excluded at step -1)
unsigned int factors[25];

map<unsigned long long, unsigned long long> minimal_k;

//Recursive simplex generation
void pascal_primes_simplex(unsigned int const current_factors_position, unsigned long long current_number,
        unsigned long long current_permutation_value, unsigned long long const current_sum_value){
    //we update the minimal_k if needed
    if(minimal_k.count(current_permutation_value) == 0 || minimal_k[current_permutation_value] > current_number)
        minimal_k[current_permutation_value] = current_number;

    //We generate for the remaining factors the upper partial simplex (we add the new possible kj values)
    for(unsigned int kj = 1; kj <= factors[current_factors_position-1];kj++){
        //We each time adds the current prime to the number we are working on
        current_number*= primes[current_factors_position];
        if(current_number >= MAXValue)
            break;
        //We now compute the new permutation value which is (k1+...+k(j-1);k1 .. k(j-1))*(k1+...+kj; kj)
        //As we move the value of kj by one at a time the only part that changes is (k1+...+kj; kj)
        //So we are going from (k1 + ... + kj; kj) to (k1 + ... + kj + 1 ; kj + 1)
        //Hence, (k1 + ... + kj; kj)*(k1 +... + kj+1)/(kj+1) = (k1 + ... + kj + 1 ; kj + 1)
        //NB : as here kj is the loop indices no +1 is visible
        current_permutation_value *=(current_sum_value+kj);
        current_permutation_value /=kj;
        factors[current_factors_position]++;
        if(current_permutation_value >= MAXValue)
            break;
        pascal_primes_simplex(current_factors_position+1,current_number, current_permutation_value,current_sum_value+kj);
    }
}

int main(){
    //generate all the needed simplex
    //This is the time intensive part
    //We compute all values with a base value of
    unsigned long long current_number = 1;
    for(unsigned long long kj = 1; current_number < MAXValue;kj++){
        current_number *= 2;
        factors[0]++;
        pascal_primes_simplex(1, current_number, 1, kj);
    }
    //Now we have everything needed
    long long newValueTofind;
    while(scanf("%lld",&newValueTofind)!=EOF){
        printf("%lld %lld\n",newValueTofind,minimal_k[newValueTofind]);
    }
    return 0;
}


