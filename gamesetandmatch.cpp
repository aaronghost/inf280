//Probability by case disjunctions and other satanic trickeries
//Done by intelligent people on this website
//http://datagenetics.com/blog/august12018/index.html
//But we will be providing an algorithm for this calculation

#include <iostream>

using namespace std;

long double computeGameProbability(int pAScore, int pBScore,long double probability){
    if(pAScore == 4 && pAScore - pBScore >= 2)
        return 1;
    else if(pBScore == 4 && pBScore - pAScore >= 2)
        return 0;
    else if(pAScore == pBScore && pAScore == 3)
        return probability*probability/(1-2*probability*(1-probability));
    else
        return computeGameProbability(pAScore+1,pBScore,probability)*probability+computeGameProbability(pAScore,pBScore+1,probability)*(1-probability);
}

long double computeTieProbability(int pAScore, int pBScore,long double probability){
    if(pAScore == 7 && pAScore -pBScore >= 2)
        return 1;
    else if(pBScore == 7 && pBScore -pAScore >= 2)
        return 0;
    else if(pAScore == pBScore && pAScore == 6)
        return probability*probability/(1-2*probability*(1-probability));
    else
        return computeTieProbability(pAScore+1,pBScore,probability)*probability+computeTieProbability(pAScore,pBScore+1,probability)*(1-probability);
}

long double computeSetProbability(int pAScore, int pBScore, long double probability, long double gameProbability, long double tieProbability){
    if(pAScore >= 6 && pAScore-pBScore >= 2)
        return 1;
    else if(pBScore >= 6 && pBScore - pAScore >= 2)
        return 0;
    else if(pAScore == pBScore && pAScore == 6)
        return tieProbability;
    else
        return computeSetProbability(pAScore+1,pBScore,probability,gameProbability,tieProbability)*gameProbability
                + computeSetProbability(pAScore,pBScore+1,probability,gameProbability, tieProbability)*(1-gameProbability);
}

long double computeMatchProbability(int pAScore, int pBscore, long double setProbability){
    if(pAScore == 2)
        return 1;
    else if(pBscore == 2)
        return 0;
    else
        return computeMatchProbability(pAScore+1,pBscore,setProbability)*setProbability
                + computeMatchProbability(pAScore,pBscore+1,setProbability)*(1-setProbability);
}

int main(){
    long double probability;
    while(scanf("%Lf",&probability) != EOF){
        if(probability < 0)
            break;
        long double gameProbability = computeGameProbability(0,0,probability);
        long double tieProbability = computeTieProbability(0,0,probability);
        long double setProbability = computeSetProbability(0,0, probability, gameProbability, tieProbability);
        long double matchProbability = computeMatchProbability(0,0,setProbability);
        printf("%Lf %Lf %Lf\n",gameProbability,setProbability,matchProbability);
    }
    return 0;
}