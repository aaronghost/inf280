#include <iostream>
#include <queue>
#include <deque>
using namespace std;

int main(){
    unsigned int treeNumber, linkNumber;
    static uint32_t parentState[(1 <<21)];
    static uint32_t shootFromParent[(1 <<21)];
    while(scanf("%u %u",&treeNumber, &linkNumber) != EOF) {
        uint32_t adjacencyMatrix[21];
        if (treeNumber == 0 && linkNumber == 0)
            break;

        for(unsigned int i = 0; i < (1 << 21);i++){
            parentState[i] = 0;
        }
        for(unsigned int i = 0; i < 21;i++){
            adjacencyMatrix[i] = 0;
        }
        unsigned int element1, element2;
        for(unsigned int i = 0; i < linkNumber ; i++){
            scanf("%u %u",&element1, &element2);
            adjacencyMatrix[element1] = adjacencyMatrix[element1] | (1<<element2);
            adjacencyMatrix[element2] = adjacencyMatrix[element2] | (1<<element1);
        }
        //Perform a BFS
        const uint32_t allTrees = (1<<treeNumber)-1;
        queue<uint32_t> stateQue;
        stateQue.push(allTrees);
        uint32_t currentState;

        while(!stateQue.empty()){
            currentState = stateQue.front();
            stateQue.pop();
            if(currentState == 0)
                break;

            //Shoot
            for(unsigned int i = 0; i < treeNumber; i++){
                uint32_t newStateAfterShoot = currentState & ~(1 << i);
                //Monkey jump
                uint32_t newStateAfterMove = 0;
                for(unsigned int j = 0; j < treeNumber; j++){
                    //Condition only match the value of the j bits
                    if(newStateAfterShoot & (1 << j))
                        newStateAfterMove = newStateAfterMove | adjacencyMatrix[j];
                }
                if(parentState[newStateAfterMove] == 0) {
                    stateQue.push(newStateAfterMove);
                    shootFromParent[newStateAfterMove] = i;
                    parentState[newStateAfterMove] = currentState;
                }
            }
        }
        if(currentState != 0)
            printf("Impossible\n");
        else{
            //Construct back a solution
            deque<uint32_t> solutionQue;
            while(currentState != allTrees){
                solutionQue.push_front(shootFromParent[currentState]);
                currentState = parentState[currentState];
            }
            printf("%lu:",solutionQue.size());
            for(auto solutionItem : solutionQue){
                printf(" %u",solutionItem);
            }
            printf("\n");
        }
    }
    return 0;
}