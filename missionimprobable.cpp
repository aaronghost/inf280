#include <iostream>
#include <vector>
#include <limits>
#include <queue>

using namespace std;

//Hopcroft-Karp Algorithm
// Artificial node (unused otherwise) -- end of augmenting path
#define NIL 0
// "Infinity", i.e., value larger than min(|X|, |Y|)
#define INF (numeric_limits<unsigned long long>::max)()

#define MAXX 101

// Neighbors in Y of nodes in X
vector<int> Adj[MAXX];

// Matching X-Y and Y-X
int PairX[MAXX];
int PairY[MAXX];

// Augmenting path lengths
unsigned long long Dist[MAXX];

bool DFS(int const x) {
    if (x == NIL)
        return true; // reached NIL
    for (int const y : Adj[x])
        if (Dist[PairY[y]] == Dist[x] + 1 && DFS(PairY[y])) { // follow trace of BFS
            PairX[x] = y; // add edge from x to y to matching
            PairY[y] = x;
            return true;
        }
    Dist[x] = INF;
    return false; // no augmenting path found
}

bool BFS(vector<int> const & X) {
    queue<int> Q;
    Dist[NIL] = INF;
    for(auto x : X) { // start from nodes that are not yet matched
        Dist[x] = (PairX[x] == NIL) ? 0 : INF;
        if (PairX[x] == NIL)
            Q.push(x);
    }
    while (!Q.empty()) { // find all shortest paths to NIL
        int x = Q.front();
        Q.pop();
        if (Dist[x] < Dist[NIL]) // can this become a shorter path?
            for (auto y : Adj[x])
                if (Dist[PairY[y]] == INF) {
                    Dist[PairY[y]] = Dist[x] + 1; // update path length
                    Q.push(PairY[y]);
                }
    }
    return Dist[NIL] != INF; // any shortest path to NIL found?
}

int HopcroftKarp(vector<int> const & X, vector<int> const & Y) {
    //Initialize empty matching
    for (int const x : X)
        PairX[x] = NIL;
    for (int const y : Y)
        PairY[y] = NIL;

    int Matching = 0; // count number of edges in matching
    while (BFS(X)) { // find all shortest augmenting paths
        for(auto x : X) // update matching cardinality
            if (PairX[x] == NIL && // node not yet in matching?
                DFS(x)) // does an augmenting path start at x?
                Matching++;
    }
    return Matching;
}

int main(){
    unsigned int r, c;
    while(scanf("%u %u",&r, &c) != EOF){
        vector<vector<bool>> top_camera(r, vector<bool>(c, false));
        vector<unsigned long long> front_camera(c, 0);
        vector<unsigned long long> side_camera(r,0);
        unsigned long long totalCrates = 0;
        unsigned long long remainingCrates = 0;
        unsigned long long value;
        //Parse the cameras
        for(unsigned int x = 0; x < r; x++){
            for(unsigned int y = 0 ; y < c; y++){
                scanf("%llu",&value);
                top_camera[x][y] = (value != 0);
                front_camera[y] = max(front_camera[y],value);
                side_camera[x] = max(side_camera[x],value);
                totalCrates += value;
                remainingCrates += top_camera[x][y];
            }
        }
        for(int x = 0 ; x < MAXX ; x++)
            Adj[x].clear();

        for(unsigned int x = 0 ; x < c ; x++)
            for(unsigned int y = 0 ; y < r ; y++)
                if(top_camera[y][x] && front_camera[x] == side_camera[y])
                    //NIL is 0 so we have to move things from 1
                    Adj[x+1].push_back((y+1));
        vector<int> X, Y;
        unsigned int x = 1, y = 1;
        for(unsigned int camera_x : front_camera){
            if(camera_x) {
                X.push_back(x);
                remainingCrates += camera_x - 1;
            }
            x++;
        }
        for(unsigned camera_y : side_camera){
            if(camera_y)
                Y.push_back(y);
            y++;
        }
        HopcroftKarp(X,Y);
        for(unsigned int y : Y)
            if(PairY[y] == NIL)
                remainingCrates += side_camera[y-1]-1;
        printf("%llu\n",totalCrates-remainingCrates);
    }
    return 0;
}