#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <algorithm>

using namespace std;

vector<pair<int, pair<int,int> > > Edges;
set<pair<int,int> > A;

map<int, pair<int,unsigned int> > Sets;  // map to parent & rank
void MakeSet(int x) {
    Sets.insert(make_pair(x, make_pair(x, 0)));
}
int Find(int x) {
    if(Sets[x].first == x)
        return x;              // Parent == x ?
    else
        return Sets[x].first = Find(Sets[x].first);  // Get Parent
}

void Union(int x,int y) {
    int parentX = Find(x), parentY = Find(y);
    int rankX = Sets[parentX].second, rankY = Sets[parentY].second;
    if(parentX == parentY)
        return;
    else if(rankX < rankY)
        Sets[parentX].first = parentY;
    else
        Sets[parentY].first = parentX;
    if(rankX == rankY)
        Sets[parentX].second++;
}

// Final minimum spanning tree
void Kruskal(int citiesNumber) {
    for(int u=0; u < citiesNumber ; u++)
        MakeSet(u);                         // Initialize Union-Find
    sort(Edges.begin(), Edges.end());      // Sort edges by weight
    for (auto tmp : Edges) {
        auto edge = tmp.second;
        if (Find(edge.first) != Find(edge.second)) {
            Union(edge.first, edge.second);       // update Union-Find
            A.insert(edge);                     // include edge in MST
        }
    }
}

int main(){
    unsigned int testCaseNumber;
    scanf("%u",&testCaseNumber);
    for(unsigned int i = 1 ; i <= testCaseNumber ; i++){
        Edges.clear();
        A.clear();
        Sets.clear();
        unsigned int citiesNumber;
        scanf("%u",&citiesNumber);
        vector<unsigned int> cities = vector<unsigned int>(citiesNumber*citiesNumber);
        for(unsigned int j = 0 ; j < citiesNumber ; j++){
            for(unsigned int k = 0 ; k < citiesNumber ; k++){
                unsigned int pathCost;
                char useless[2];
                scanf("%u%c",&pathCost, &useless[0]);
                cities[j*citiesNumber+k] = pathCost;
                if(pathCost != 0)
                    Edges.emplace_back(make_pair(pathCost,make_pair(j,k)));
            }
        }
        Kruskal(citiesNumber);
        printf("Case %u:\n",i);
        vector<pair<int,pair<char,char>>> remainingEdges;
        for(auto a : A) {
            remainingEdges.emplace_back(make_pair(cities[a.first * citiesNumber + a.second],
                                                  make_pair(char(65 + a.first), char(65 + a.second))));
        }
        sort(remainingEdges.begin(),remainingEdges.end());
        for(auto remaining : remainingEdges)
            printf("%c-%c %d\n",remaining.second.first, remaining.second.second,remaining.first);
    }
    return 0;
}
