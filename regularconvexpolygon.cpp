#include <iostream>
#include <cmath>

using namespace std;

#define EPSILON 1e-5


//Point / Vector structure
struct Point{
    long double x,y;

    //Vector from two points
    Point operator-(const Point &other) const{
        return {x-other.x,y-other.y};
    }

    long double scalar(const Point &other) const{
        return x*other.x + y*other.y;
    }

    long double norm() const{
        return hypot(x,y);
    }

    Point()= default;

    Point(long double _x, long double _y):x(_x),y(_y){}
};

//the modulo is close from zero from lower or upper (fabs could also work)
bool angle_division(long double angle_to_divide,long double dividing_angle){
    return (fmod(angle_to_divide,dividing_angle) < EPSILON) || ((dividing_angle-fmod(angle_to_divide,dividing_angle)) < EPSILON);
}

int main(){
    const long double pi = acos(-1);
    //definition of the scalar product : u.v = ||u||*||v|| cos(u,v)
    //cos(u,v) = u.v/(||u||*||v||)
    //angle(u,v) = acos(u.v/(||u||*||v||))
    Point p1,p2,p3;
    while(scanf("%Lf %Lf %Lf %Lf %Lf %Lf",&p1.x,&p1.y,&p2.x,&p2.y,&p3.x,&p3.y) == 6){
        Point p2p1 = p1-p2;
        Point p2p3 = p3-p2;
        Point p3p1 = p1-p3;
        //three angles from the triangle
        //They are related to the angles of the convex regular polygon through the "Center angle Theorem"
        //We do not know the exact coefficient of proportionality for each, but if an angle divides the three angles of the
        //triangle, then we can build a convex regular polygon with this angle
        //And as we enumerate them from the smallest possible values of angle, we get the smallest corner polygon
        long double p1p2p3_angle = acos(p2p1.scalar(p2p3)/(p2p1.norm()*p2p3.norm()));
        long double p2p3p1_angle = acos(p2p3.scalar(p3p1)/(p2p3.norm()*p3p1.norm()));
        long double p3p1p2_angle = acos(p3p1.scalar(p2p1)/(p3p1.norm()*p2p1.norm()));
        for(unsigned int i = 3; i <= 1000;i++){
            long double angle_from_center = pi/(long double)(i);
            if(angle_division(p1p2p3_angle,angle_from_center) && angle_division(p2p3p1_angle,angle_from_center)
            && angle_division(p3p1p2_angle, angle_from_center))
            {
                printf("%d\n",i);
                break;
            }
        }
    }
    return 0;
}