#include <iostream>
#include <vector>

using namespace std;

struct package{
    int x;
    int y;
    int weight;
    package():x(0),y(0),weight(0){};
    package(int _x, int _y,int _weight):x(_x),y(_y),weight(_weight){};
};

//distance to get between two points
inline int distance(package const beginPoint, package const endPoint){
    return abs(endPoint.x - beginPoint.x) + abs(endPoint.y-beginPoint.y);
}

//Dynamic programming solution : minDistances are updated from previous minDistance values
int main(){
    int testCases;
    scanf("%d",&testCases);
    for(int i = 0 ; i<testCases ; i++){
        int maxCapacity, packageNumber;
        scanf("%d",&maxCapacity);
        scanf("%d",&packageNumber);
        vector<package> packages(packageNumber);
        vector<int> distanceLinkPackages(packageNumber);
        vector<int> distanceOriginPackage(packageNumber);
        vector<int> minDistances(packageNumber);
        int x, y, weight;
        package originPackage = package();
        //Collect package and collect distance to origin
        for(int k = 0 ; k < packageNumber ; k++) {
            scanf("%d %d %d", &x, &y, &weight);
            packages[k]=package(x,y,weight);
            distanceOriginPackage[k] = distance(packages[k],originPackage);
        }
        //Compute distances between packages following each others
        for(int k = 0 ; k < packageNumber - 1 ; k++){
            distanceLinkPackages[k] = distance(packages[k],packages[k+1]);
        }
        //Initialization
        minDistances[0] = 2*distanceOriginPackage[0];
        //Considering all points
        for(int j = 1 ; j< packageNumber; j++){
            int minDistance = 2*distanceOriginPackage[j]+minDistances[j-1];
            int packagesWeight = packages[j].weight;
            //Considering all direct travels which ends to the point
            for(int k = j-1 ; k >= 0 ; k--){
                packagesWeight += packages[k].weight;
                if(packagesWeight > maxCapacity)
                    break;
                int currentDistance = distanceOriginPackage[k]+distanceOriginPackage[j];
                for(int l = k; l < j ;l++){
                    currentDistance += distanceLinkPackages[l];
                }
                if(k > 0)
                    currentDistance += minDistances[k-1];
                minDistance = min(currentDistance,minDistance);
            }
            minDistances[j] = minDistance;
        }
        printf("%d\n",minDistances[packageNumber-1]);
        if(i != testCases -1)
            printf("\n");
    }
    return 0;
}

