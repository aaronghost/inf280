#include <iostream>

using namespace std;

int main(){
    unsigned int cellsNumber;
    while(scanf("%u",&cellsNumber) != EOF){
        bool cellsPath[52][52];
        for(unsigned int i = 0 ; i < 52; i++)
            for(unsigned int j = 0 ; j< 52 ; j++)
                cellsPath[i][j] = false;
        char word[9];
        //Build paths in cells
        for(unsigned int i = 0 ; i < cellsNumber ; i++){
            scanf("%8s",&word[0]);
            for(unsigned int j = 0; j < 4 ; j++){
                for(unsigned int k = 0 ; k < 4 ; k++){
                    if(j != k && word[2*j] != '0' && word[2*k] != '0'){
                        unsigned int begin = (word[2*j]-'A');
                        unsigned int end  = (word[2*k] - 'A');
                        if(word[2*j+1] == '+') {
                            begin += 26;
                        }
                        //accepting bounds in other cells
                        if(word[2*k+1] == '-'){
                            end += 26;
                        }
                        cellsPath[begin][end] = true;
                    }
                }
            }
        }

        //Floyd Warshall (calculates if a path exist to oneself)
        for(unsigned int k=0; k < 52; k++)      // check sub-path combinations
            for(unsigned int i=0; i < 52; i++)
                for(unsigned int j=0; j < 52; j++)
                    cellsPath[i][j] = cellsPath[i][j] || (cellsPath[i][k] && cellsPath[k][j]);
        bool unbounded = false;
        //Check the diagonal
        for(unsigned int i = 0 ; i < 52 ; i++){
            if(cellsPath[i][i]){
                unbounded = true;
                break;
            }
        }
        if(unbounded)
            printf("unbounded\n");
        else
            printf("bounded\n");
    }
    return 0;
}