#include <iostream>
using namespace std;
int main(){
    int width = 0;
    while(scanf("%d",&width) != EOF) {
        int shatteredPiecesNumber;
        scanf("%d",&shatteredPiecesNumber);
        int totalCakeArea = 0;
        for (int i = 0; i < shatteredPiecesNumber; i++) {
            int pieceWidth, pieceHeight;
            scanf("%d",&pieceWidth);
            scanf("%d",&pieceHeight);
            totalCakeArea += pieceWidth * pieceHeight;
        }
        printf("%d\n",totalCakeArea / width);
    }
}