#include <iostream>
#include <list>

using namespace std;

struct HardDrive{
    long formerCapacity,newCapacity;
    HardDrive():formerCapacity(0),newCapacity(0){};
    bool operator< (const HardDrive& other) const{
        if(formerCapacity<newCapacity)
            return formerCapacity<other.formerCapacity;
        else
            return -newCapacity<-other.newCapacity;
    }
};

int main(){
    int hardDrivesNumber;
    while(scanf("%d",&hardDrivesNumber) != EOF){
        list<HardDrive> biggerCapList;
        list<HardDrive> fewerCapList;
        long requiredSpace = 0;
        for(int i = 0 ; i < hardDrivesNumber ; i++) {
            HardDrive hardDrive;
            scanf("%ld %ld", &hardDrive.formerCapacity, &hardDrive.newCapacity);
            if (hardDrive.newCapacity > hardDrive.formerCapacity) {
                biggerCapList.push_back(hardDrive);
            } else {
                fewerCapList.push_back(hardDrive);
            }
        }
        biggerCapList.sort();
        fewerCapList.sort();
        long available = 0;
        for(int i = 0; i < hardDrivesNumber; i++){
            HardDrive hardDrive;
            if(!biggerCapList.empty()) {
                hardDrive = biggerCapList.front();
                biggerCapList.pop_front();
            }else{
                hardDrive = fewerCapList.front();
                fewerCapList.pop_front();
            }
            if(hardDrive.formerCapacity <= available){
                available += hardDrive.newCapacity-hardDrive.formerCapacity;
            } else{
                requiredSpace += hardDrive.formerCapacity - available;
                available = hardDrive.newCapacity;
            }
        }
        printf("%ld\n",requiredSpace);
    }
}