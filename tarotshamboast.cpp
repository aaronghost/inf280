#include <iostream>
#include <cstring>
#include <queue>
#include <algorithm>

using namespace std;

#define MAXN 100001

char p[10][MAXN];
unsigned long T[MAXN+1];

void KMPtable(const unsigned long np,unsigned int j){
    T[0] = -1;
    long cnd = 0;
    for (unsigned long i = 1; i <= np; i++)
    {
        T[i] = cnd;
        while (cnd >= 0 && p[j][cnd] != p[j][i])
            cnd = T[cnd];
        cnd++;
    }
}

int main(){
    unsigned long roundNumber;
    unsigned int predictionNumber;
    while(scanf("%lu %u",&roundNumber, &predictionNumber) != EOF){
        vector<pair<vector<unsigned long>,int>> allCycles;
        for(unsigned int i = 0; i < predictionNumber;i++){
            scanf("%s",p[i]);
            const unsigned long np =strlen(p[i]);
            //KMP pre-computing phase
            KMPtable(np,i);
            unsigned long prefix = np;
            vector<unsigned long> currentCycles;
            do{
                prefix = T[prefix];
                //checking that what we found was of a correct length or no cycle was found
                if ((long) roundNumber >= 2 * (long)np - (long) prefix || prefix == 0) {
                    currentCycles.push_back(prefix);
                }
            } while(prefix != 0);
            allCycles.emplace_back(make_pair(currentCycles,i));
        }
        sort(allCycles.begin(),allCycles.end());
        for(auto const & allCycle : allCycles){
            printf("%s\n",p[allCycle.second]);
        }
    }
    return 0;
}