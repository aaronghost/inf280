#include <iostream>
#include <list>
#include <numeric>

using namespace std;
int main(){
    int population, commandNumber;
    int caseNumber = 1;
    while(scanf("%d %d",&population, &commandNumber) != EOF) {
        if(population == 0 && commandNumber == 0){
            return 0;
        }else{
            printf("Case %d:\n",caseNumber);
            list<int> waiters(min(population,commandNumber));
            iota(waiters.begin(), waiters.end(),1);
            for(int i = 0; i < commandNumber; i++){
                char command;
                scanf("\n%c",&command);
                if(command == 'N'){
                    int waiter = waiters.front();
                    waiters.pop_front();
                    printf("%d\n",waiter);
                    waiters.push_back(waiter);
                }
                else if(command == 'E'){
                    int urgencyWaiter;
                    scanf("%d",&urgencyWaiter);
                    waiters.remove(urgencyWaiter);
                    waiters.push_front(urgencyWaiter);
                }
            }
            caseNumber++;
        }
    }
}