#include <iostream>
#include <stack>
#include <vector>
#include <list>
#include <algorithm>

using namespace std;

//Eulerian cycle problem!!!!!
int main(){
    unsigned int caseNumber;
    scanf("%u",&caseNumber);
    for(unsigned int i = 1 ; i <= caseNumber ; i++){
        printf("Case #%u\n",i);
        list<unsigned int> paths[50];
        unsigned int beadsNumber;
        unsigned int anyNodeThatExist = 0;
        scanf("%u",&beadsNumber);
        //Create adjacency list
        for(unsigned int j = 0 ; j < beadsNumber ; j++){
            unsigned int point1, point2;
            scanf("%u %u",&point1, &point2);
            paths[point1-1].push_back(point2-1);
            paths[point2-1].push_back(point1-1);
        }
        //Eulerian theorem verification
        bool someBeadsLost = false;
        for(unsigned int j = 0 ; j < 50 ; j++){
            if(!paths[j].empty())
                anyNodeThatExist = j;
            if(paths[j].size() % 2 != 0){
                printf("some beads may be lost\n\n");
                someBeadsLost = true;
                break;
            }
        }
        if(someBeadsLost)
            continue;
        vector<unsigned int> rep;
        //We know that now we can find a cycle
        stack<unsigned int> nodesInAlgorithm;
        nodesInAlgorithm.push(anyNodeThatExist);
        while(!nodesInAlgorithm.empty()){
            unsigned int currentNode = nodesInAlgorithm.top();
            if(paths[currentNode].empty()){
                rep.push_back(currentNode);
                nodesInAlgorithm.pop();
            }
            else{
             unsigned int nextNode = paths[currentNode].front();
             paths[currentNode].pop_front();
             auto it = find(paths[nextNode].begin(),paths[nextNode].end(),currentNode);
             paths[nextNode].erase(it);
             nodesInAlgorithm.push(nextNode);
            }
        }
        for(unsigned int k = 0 ; k < rep.size()-1; k++){
            printf("%u %u\n",rep[k]+1,rep[k+1]+1);
        }
        printf("\n");
    }
    return 0;
}